﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DddInfrastructure.EntityFrameworkCore
{
    public abstract class AggregateDbContext<TAggregateDbContext, TAggregateRoot> : DbContext
        where TAggregateRoot : DomainAggregateRoot
        where TAggregateDbContext: AggregateDbContext<TAggregateDbContext, TAggregateRoot>
    {
        private readonly IDomainEventBus _domainEventBus;
        
        public DbSet<TAggregateRoot> Items { get; protected set; }

        protected AggregateDbContext(DbContextOptions<TAggregateDbContext> dbContextOptions, IDomainEventBus domainEventBus) : base(dbContextOptions)
        {
            _domainEventBus = domainEventBus;
        }

        public override int SaveChanges()
        {
            var domainEvents = ExtractAllDomainEvents();
            var saveChangesResult = base.SaveChanges();
            EmitAllDomainEventsAsync(domainEvents).GetAwaiter().GetResult();
            return saveChangesResult;
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            var domainEvents = ExtractAllDomainEvents();
            var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            await EmitAllDomainEventsAsync(domainEvents);
            return result;
        }

        private IReadOnlyList<IDomainEvent> ExtractAllDomainEvents()
        {
            var domainEvents = ChangeTracker
                .Entries<TAggregateRoot>()
                .SelectMany(entry => entry.Entity.PendingEvents)
                .OrderBy(domainEvent => domainEvent.EmittedTime)
                .ToImmutableList();

            return domainEvents;
        }

        private Task EmitAllDomainEventsAsync(IReadOnlyList<IDomainEvent> domainEvents)
        {
            return Task.WhenAll(domainEvents.Select(domainEvent => _domainEventBus.PublishAsync(domainEvent)));
        }
    }
}
