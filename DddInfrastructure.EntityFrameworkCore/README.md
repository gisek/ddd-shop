﻿# About

This is a set of tools needed for Domain Driven Design implementation. It's EntityFramework Core specific.
It's agnostic against:
* database
* message broker
* web framework