﻿using System;
using Microsoft.EntityFrameworkCore;

namespace DddInfrastructure.EntityFrameworkCore
{
    public class DbContextFactory
    {
        private readonly Func<Type, object> _creator;

        public DbContextFactory(Func<Type, object> creator)
        {
            _creator = creator;
        }

        public TContext CreateTransient<TContext>() where TContext : DbContext
        {
            return (TContext)_creator(typeof(TContext));
        }
    }
}