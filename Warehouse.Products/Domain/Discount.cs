﻿using System.Collections.Generic;
using CSharpFunctionalExtensions;
using DddInfrastructure;

namespace Warehouse.Products.Domain
{
    internal class Discount : DomainValueObject
    {
        public int PercentValue { get; private set; }

        private Discount(int percentValue)
        {
            PercentValue = percentValue;
        }

        public static Result<Discount> Create(int percent)
        {
            if (percent > 100)
            {
                return Result.Fail<Discount>("Discount cannot be larger than 100");
            }

            if (percent < 0)
            {
                return Result.Fail<Discount>("Discount cannot be negative");
            }

            return Result.Ok(new Discount(percent));
        }

        public static implicit operator int(Discount discount)
        {
            return discount.PercentValue;
        }

        public static implicit operator double(Discount discount)
        {
            return discount.PercentValue / 100d;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return PercentValue;
        }
    }
}