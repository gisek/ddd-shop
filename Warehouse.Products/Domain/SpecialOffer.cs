﻿using DddInfrastructure;

namespace Warehouse.Products.Domain
{
    internal class SpecialOffer : DomainEntity
    {
        public SpecialOffer(string code, Discount discount)
        {
            Code = code;
            Discount = discount;
        }

        private SpecialOffer() { } // ef core cannot resolve an owned type by constructor

        public string Code { get; private set; }

        public Discount Discount { get; private set; }
    }
}