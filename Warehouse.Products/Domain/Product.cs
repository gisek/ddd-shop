﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Common;
using CSharpFunctionalExtensions;
using DddInfrastructure;
using Warehouse.Domain.Common.DomainEvents.Products;

namespace Warehouse.Products.Domain
{
    internal class Product : DomainAggregateRoot
    {
        public string Name { get; set; }

        public Product(string name) : base(new ProductCreated(name))
        {
            Name = name;
        }

        private readonly List<SpecialOffer> _specialOffers = new List<SpecialOffer>();
        public IReadOnlyList<SpecialOffer> SpecialOffers => _specialOffers.ToImmutableList();

        public void ActivateSpecialOffer(Discount discount, string promoCode)
        {
            _specialOffers.Add(new SpecialOffer(promoCode, discount));
            Emit(new NewPromotionActivated(discount, promoCode));
        }

        public void RevokeSpecialOffer(string promoCode)
        {
            _specialOffers
                .SingleOrDefault(p => p.Code == promoCode)
                .ToMaybe()
                .Execute(promotion => _specialOffers.Remove(promotion));

            Emit(new SpecialOfferRevoked(promoCode));

            if (_specialOffers.IsEmpty())
            {
                Emit(new NoPromotionsOnProduct(Id));
            }
        }
    }
}