﻿using DddInfrastructure;
using DddInfrastructure.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Products.Domain;

namespace Warehouse.Products
{
    internal class ProductsDbContext : AggregateDbContext<ProductsDbContext, Product>
    {
        public ProductsDbContext(DbContextOptions<ProductsDbContext> dbContextOptions, IDomainEventBus domainEventBus) : base(dbContextOptions, domainEventBus)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>().Metadata.FindNavigation(nameof(Product.SpecialOffers)).SetPropertyAccessMode(PropertyAccessMode.Field);

            ConfigureProduct(modelBuilder.Entity<Product>());
            ConfigureSpecialOffer(modelBuilder.Entity<SpecialOffer>());
        }

        private void ConfigureSpecialOffer(EntityTypeBuilder<SpecialOffer> builder)
        {
            builder.ToTable("SpecialOffers", "products");
            builder.OwnsOne(p => p.Discount);
        }

        private void ConfigureProduct(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products", "products");
            builder.HasMany(p => p.SpecialOffers).WithOne();
        }
    }
}
