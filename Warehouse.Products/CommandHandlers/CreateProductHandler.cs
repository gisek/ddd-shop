﻿using System;
using System.Threading.Tasks;
using DddInfrastructure;
using DddInfrastructure.EntityFrameworkCore;
using Warehouse.Domain.Common.Commands;
using Warehouse.Products.Domain;

namespace Warehouse.Products.CommandHandlers
{
    internal class CreateProductHandler : ICommandHandler<CreateProduct>
    {
        private readonly DbContextFactory _contextFactory;

        public CreateProductHandler(DbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task Handle(CreateProduct command)
        {
            await Task.Delay(TimeSpan.FromSeconds(5)); // todo ag remove later, now it simulates a long running job
            using (var context = _contextFactory.CreateTransient<ProductsDbContext>())
            {
                context.Items.Add(new Product(command.Name));
                await context.SaveChangesAsync();
            }
        }
    }
}