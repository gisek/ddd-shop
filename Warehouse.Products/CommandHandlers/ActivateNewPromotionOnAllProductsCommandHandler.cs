﻿using System.Threading.Tasks;
using Common.Infrastructure;
using Common.Infrastructure.Events;
using CSharpFunctionalExtensions;
using DddInfrastructure;
using Warehouse.Domain.Common.Commands;
using Warehouse.Products.Domain;

namespace Warehouse.Products.CommandHandlers
{
    internal class ActivateNewPromotionOnAllProductsCommandHandler : ICommandHandler<ActivateNewPromotionOnAllProducts>
    {
        private readonly ProductsDbContext _context;
        private readonly IEventBus _eventBus;

        public ActivateNewPromotionOnAllProductsCommandHandler(ProductsDbContext context, IEventBus eventBus)
        {
            _context = context;
            _eventBus = eventBus;
        }

        public async Task Handle(ActivateNewPromotionOnAllProducts command)
        {
            await Discount.Create(command.PercentDiscount)
                .OnFailure(async error => await _eventBus.PublishAsync(new FailedToAddAPromotion(error)) )
                .OnSuccess(async discount =>
                {
                    foreach (var product in _context.Items)
                    {
                        product.ActivateSpecialOffer(discount, command.PromoCode);
                    }

                    await _context.SaveChangesAsync();
                });
        }
    }
}