﻿using System.Threading.Tasks;
using DddInfrastructure;
using Microsoft.Extensions.Logging;
using Warehouse.Domain.Common.DomainEvents.Products;

namespace Warehouse.Products.DomainEventHandlers
{
    public class ProductCreatedHandler : IDomainEventHandler<ProductCreated>
    {
        private readonly ILogger _logger;

        public ProductCreatedHandler(ILogger<ProductCreatedHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(ProductCreated domainEvent)
        {
            _logger.LogInformation($"Product created: {domainEvent.Name} at: {domainEvent.EmittedTime.UtcTicks}");
            return Task.CompletedTask;
        }
    }
}