﻿using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;

namespace Common
{
    public static class UsefulExtensions
    {
        public static Maybe<T> ToMaybe<T>(this T value) => Maybe<T>.From(value);

        public static bool IsEmpty<T>(this IEnumerable<T> enumerable) => !enumerable.Any();

        public static IEnumerable<T> ToEnumerable<T>(this T value) => new List<T>() {value};
    }
}