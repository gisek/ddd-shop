﻿# Running migrations

```
cd Shop.Web
dotnet ef migrations add Migration1 --context ProductsDbContext -o Migrations/Products
dotnet ef migrations add Migration1 --context EmployeesDbContext -o Migrations/Employees
```