﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Features.OwnedInstances;
using Common;
using Common.Infrastructure;
using Common.WebUtils;
using Common.WebUtils.AutofacModules;
using DddInfrastructure;
using DddInfrastructure.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Warehouse.Employees;
using Warehouse.Products;

namespace Shop.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            RegisterDbContexts(services);

            var builder = new ContainerBuilder();
            builder.Populate(services);

            RegisterSpecificTypes(builder);

            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            MigrateDatabases();
        }

        private void MigrateDatabases()
        {
            IEnumerable<Type> ContextsToMigrate()
            {
                yield return typeof(ProductsDbContext);
                yield return typeof(EmployeesDbContext);
            }

            foreach (var contextType in ContextsToMigrate())
            {
                using (var context = (DbContext) ApplicationContainer.Resolve(contextType))
                {
                    context.Database.Migrate();
                }
            }
        }

        private static void RegisterSpecificTypes(ContainerBuilder builder)
        {
            // In-memory bus should be a singleton. It's lifespan should NOT depend on request time.
            // Is has to live beyond the request scope, because it holds background tasks (fire and forget).
            // Background tasks execution depends on IComponentContext which when disposed disposes every object created with it.
            // It would kill our background tasks (handlers).
            builder.Register(componentContext =>
                {
                    var c = componentContext.Resolve<IComponentContext>();
                    var l = c.Resolve<ILogger<InMemoryBus>>();
                    return new InMemoryBus(t =>
                    {
                        c.TryResolve(t, out var handler);
                        return handler.ToMaybe();
                    }, l);
                })
                .As(typeof(IDomainEventBus), typeof(IEventBus), typeof(ICommandBus))
                .SingleInstance();

            builder.Register(componentContext =>
            {
                var c = componentContext.Resolve<IComponentContext>();
                return new DbContextFactory(type =>
                {
                    // Using Owned disables Autofac from disposing DbContext automatically
                    // The purpose of this factory is to manage the DbContext lifetime on our own
                    dynamic ownedContext = c.Resolve(typeof(Owned<>).MakeGenericType(type));
                    return ownedContext.Value;
                });
            });

            builder.RegisterModule<DomainEventsHandlersModule>();
            builder.RegisterModule<IntegrationEventsHandlersModule>();
            builder.RegisterModule<CommandsHandlersModule>();
        }

        private static void RegisterDbContexts(IServiceCollection services)
        {
            services.AddDbContext<ProductsDbContext>(optionsBuilder
                => optionsBuilder.UseSqlServer(
                    @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Temp\TmpDb.mdf;Integrated Security=True;Connect Timeout=30",
                    o => o.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName)));

            services.AddDbContext<EmployeesDbContext>(optionsBuilder
                => optionsBuilder.UseSqlServer(
                    @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Temp\TmpDb.mdf;Integrated Security=True;Connect Timeout=30",
                    o => o.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName)));
        }
    }
}
