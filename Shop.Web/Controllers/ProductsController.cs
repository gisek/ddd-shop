﻿using System;
using System.Threading.Tasks;
using DddInfrastructure;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Domain.Common.Commands;

namespace Shop.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ICommandBus _commandBus;

        public ProductsController(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        [HttpGet("create/{name}")]
        public async Task<ActionResult> Create([FromRoute] string name)
        {
            await _commandBus.Send(new CreateProduct(name)).ConfigureAwait(false);
            return Ok($"Product creation requested {DateTime.UtcNow.Ticks}");
        }

        [HttpGet("{id}/activate-promotion")]
        public async Task<ActionResult> ActivatePromotion([FromQuery] string promoCode, [FromQuery] int percentValue)
        {
            await _commandBus.Send(new ActivateNewPromotionOnAllProducts(promoCode, percentValue)).ConfigureAwait(false);
            return Accepted();
        }
    }
}
