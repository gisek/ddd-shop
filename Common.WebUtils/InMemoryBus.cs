﻿using System;
using System.Threading.Tasks;
using Common.Infrastructure;
using CSharpFunctionalExtensions;
using DddInfrastructure;
using Microsoft.Extensions.Logging;

namespace Common.WebUtils
{
    public class InMemoryBus : IDomainEventBus, ICommandBus, IEventBus
    {
        private readonly Func<Type, Maybe<object>> _handlerCreator;
        private readonly ILogger _logger;

        public InMemoryBus(Func<Type, Maybe<object>> handlerCreator, ILogger<InMemoryBus> logger)
        {
            _handlerCreator = handlerCreator;
            _logger = logger;
        }

        public Task PublishAsync(IDomainEvent domainEvent)
        {
            CallHandler(typeof(IDomainEventHandler<>), "Handle", domainEvent);
            return Task.CompletedTask;
        }

        public Task PublishAsync(IIntegrationEvent integrationIntegrationEvent)
        {
            CallHandler(typeof(IIntegrationEventHandler<>), "Handle", integrationIntegrationEvent);
            return Task.CompletedTask;
        }

        public Task Send(ICommand command)
        {
            CallHandler(typeof(ICommandHandler<>), "Handle", command);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Fire and forget
        /// </summary>
        private void CallHandler(Type handlerInterface, string handleMethodName, object argument)
        {
            Task.Run(async () =>
            {
                object handler = null;
                try
                {
                    var maybeHandler = _handlerCreator(handlerInterface.MakeGenericType(argument.GetType()));
                    if (maybeHandler.HasValue)
                    {
                        handler = maybeHandler.Value;
                        await (Task)handler.GetType().GetMethod(handleMethodName).Invoke(handler, new[] { argument });
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Handler failed unexpectedly");
                }
                finally
                {
                    (handler as IDisposable)?.Dispose();
                }
            });
        }
    }
}