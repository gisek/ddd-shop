﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using DddInfrastructure;

namespace Common.WebUtils.AutofacModules
{
    public class DomainEventsHandlersModule : EventsHandlersModuleBase
    {
        private class BulkDomainEventHandler<TDomainEvent> : IDomainEventHandler<TDomainEvent>
            where TDomainEvent : IDomainEvent
        {
            private readonly ImmutableList<IDomainEventHandler<TDomainEvent>> _handlers;

            private BulkDomainEventHandler(IEnumerable<object> handlers)
            {
                _handlers = handlers.Cast<IDomainEventHandler<TDomainEvent>>().ToImmutableList();
            }

            public async Task Handle(TDomainEvent domainEvent)
            {
                await Task.WhenAll(_handlers.Select(handler => handler.Handle(domainEvent)));
            }
        }

        protected override Type BulkEventHandlerType => typeof(BulkDomainEventHandler<>);
        protected override Type BaseHandlerInterfaceType => typeof(IDomainEventHandler<>);
    }
}