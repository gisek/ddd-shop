﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using Autofac;

namespace Common.WebUtils.AutofacModules
{
    public abstract class EventsHandlersModuleBase : HandlersModuleBase
    {
        protected abstract Type BulkEventHandlerType { get; }
        protected abstract Type BaseHandlerInterfaceType { get; }

        protected override void Load(ContainerBuilder builder)
        {
            foreach (var (handlerInterfaceType, handlerTypes) in GetEventHandlers(BaseHandlerInterfaceType))
            {
                var eventType = handlerInterfaceType.GenericTypeArguments.Single();

                foreach (var handlerType in handlerTypes)
                {
                    builder.RegisterType(handlerType).AsSelf().FindConstructorsWith(type =>
                        type.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));
                }

                builder.Register<object>(context =>
                {
                    var handlers = handlerTypes.Select(context.Resolve);

                    var bulkHandler = BulkEventHandlerType.MakeGenericType(eventType)
                        .GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Single()
                        .Invoke(new object[]{handlers});

                    return bulkHandler;
                }).As(handlerInterfaceType);
            }
        }

        private IEnumerable<(Type handlerInterfaceType, ImmutableList<Type> handlerType)> GetEventHandlers(Type handlerInterface)
        {
            var handlerTypes = GetHandlers(handlerInterface)
                .SelectMany(type => type.desiredInterfaces.Select(i => (handler: type.type, handlerInterface: i)))
                .GroupBy(tuple => tuple.handlerInterface)
                .Select(grouping => (grouping.Key, grouping.Select(tuple => tuple.handler).Distinct().ToImmutableList()));

            return handlerTypes;
        }
    }
}