﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Module = Autofac.Module;

namespace Common.WebUtils.AutofacModules
{
    public abstract class HandlersModuleBase : Module
    {
        protected IEnumerable<(Type type, IEnumerable<Type> desiredInterfaces)> GetHandlers(Type handlerInterface)
        {
            var allAssemblies = Assembly.GetEntryAssembly().GetReferencedAssemblies()
                .Concat(Assembly.GetEntryAssembly().GetName().ToEnumerable())
                .Select(Assembly.Load)
                .OrderBy(name => name.FullName)
                .ToList();

            var handlerTypes = allAssemblies
                .SelectMany(x => x.GetTypes())
                .Select(type => (
                    type: type,
                    desiredInterfaces: type.GetInterfaces().Where(i =>
                        i.IsGenericType && i.GetGenericTypeDefinition() == handlerInterface)));

            return handlerTypes;
        }
    }
}