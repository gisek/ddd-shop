﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Common.Infrastructure;

namespace Common.WebUtils.AutofacModules
{
    public class IntegrationEventsHandlersModule : EventsHandlersModuleBase
    {
        private class BulkIntegrationEventHandler<TEvent> : IIntegrationEventHandler<TEvent>
            where TEvent : IIntegrationEvent
        {
            private readonly ImmutableList<IIntegrationEventHandler<TEvent>> _handlers;

            private BulkIntegrationEventHandler(IEnumerable<object> handlers)
            {
                _handlers = handlers.Cast<IIntegrationEventHandler<TEvent>>().ToImmutableList();
            }

            public async Task Handle(TEvent @event)
            {
                await Task.WhenAll(_handlers.Select(handler => handler.Handle(@event)));
            }
        }

        protected override Type BulkEventHandlerType => typeof(BulkIntegrationEventHandler<>);
        protected override Type BaseHandlerInterfaceType => typeof(IIntegrationEventHandler<>);
    }
}