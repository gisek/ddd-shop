﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using DddInfrastructure;

namespace Common.WebUtils.AutofacModules
{
    public class CommandsHandlersModule : HandlersModuleBase
    {
        protected override void Load(ContainerBuilder builder)
        {
            foreach (var (handler, handlerInterface) in GetCommandHandlers(typeof(ICommandHandler<>)))
            {
                builder
                    .RegisterType(handler)
                    .As(handlerInterface)
                    .FindConstructorsWith(type => 
                        type.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));
            }
        }

        private IEnumerable<(Type handlerType, Type handlerInterfaceType)> GetCommandHandlers(Type handlerInterface)
        {
            var handlerTypes = GetHandlers(handlerInterface)
                .SelectMany(type => type.desiredInterfaces.Select(i => (handler: type.type, handlerInterface: i)));

            return handlerTypes;
        }
    }
}