﻿using DddInfrastructure;
using DddInfrastructure.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Warehouse.Employees
{
    internal class EmployeesDbContext : AggregateDbContext<EmployeesDbContext, Employee>
    {
        public EmployeesDbContext(DbContextOptions<EmployeesDbContext> dbContextOptions, IDomainEventBus domainEventBus) : base(dbContextOptions, domainEventBus)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Employee>()
                .ToTable("Employee", "employees");
        }
    }
}
