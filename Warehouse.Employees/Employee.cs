﻿using DddInfrastructure;
using Warehouse.Domain.Common.DomainEvents.Employees;

namespace Warehouse.Employees
{
    internal class Employee : DomainAggregateRoot
    {
        public Employee(string name) : base(new EmployeeCreatedDomainEvent(name))
        {
        }

        public string Name { get; private set; }
    }
}