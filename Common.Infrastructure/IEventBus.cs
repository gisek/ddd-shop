﻿using System.Threading.Tasks;

namespace Common.Infrastructure
{
    public interface IEventBus
    {
        Task PublishAsync(IIntegrationEvent integrationIntegrationEvent);
    }
}