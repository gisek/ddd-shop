﻿using System;

namespace Common.Infrastructure
{
    public class IntegrationEvent : IIntegrationEvent
    {
        public Guid Id { get; } = Guid.NewGuid();
        public DateTimeOffset EmittedTime { get; } = DateTimeOffset.Now;
    }
}