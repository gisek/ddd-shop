﻿using System;

namespace Common.Infrastructure
{
    public interface IIntegrationEvent
    {
        Guid Id { get; }
        DateTimeOffset EmittedTime { get; }
    }
}