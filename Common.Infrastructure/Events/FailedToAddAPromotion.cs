﻿namespace Common.Infrastructure.Events
{
    public class FailedToAddAPromotion : IntegrationEvent
    {
        public string Reason { get; }

        public FailedToAddAPromotion(string reason)
        {
            Reason = reason;
        }
    }
}