﻿using DddInfrastructure;

namespace Warehouse.Domain.Common.Commands
{
    public class CreateProduct : ICommand
    {
        public CreateProduct(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}