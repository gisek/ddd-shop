﻿using DddInfrastructure;

namespace Warehouse.Domain.Common.Commands
{
    public class ActivateNewPromotionOnAllProducts : ICommand
    {
        public string PromoCode { get; }
        public int PercentDiscount { get; }

        public ActivateNewPromotionOnAllProducts(string promoCode, int percentDiscount)
        {
            PromoCode = promoCode;
            PercentDiscount = percentDiscount;
        }

    }
}