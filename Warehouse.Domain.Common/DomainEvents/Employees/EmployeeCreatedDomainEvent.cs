﻿using DddInfrastructure;

namespace Warehouse.Domain.Common.DomainEvents.Employees
{
    public class EmployeeCreatedDomainEvent : AggregateRootCreatedDomainEvent
    {
        private readonly string _name;

        public EmployeeCreatedDomainEvent(string name)
        {
            _name = name;
        }
    }
}