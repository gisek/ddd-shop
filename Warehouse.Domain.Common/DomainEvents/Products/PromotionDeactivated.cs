﻿using DddInfrastructure;

namespace Warehouse.Domain.Common.DomainEvents.Products
{
    public class SpecialOfferRevoked : DomainEvent
    {
        public SpecialOfferRevoked(string promoCode)
        {
            PromoCode = promoCode;
        }

        public string PromoCode { get; }
    }
}