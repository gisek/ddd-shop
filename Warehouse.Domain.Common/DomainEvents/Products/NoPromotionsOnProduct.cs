﻿using System;
using DddInfrastructure;

namespace Warehouse.Domain.Common.DomainEvents.Products
{
    public class NoPromotionsOnProduct : DomainEvent
    {
        public NoPromotionsOnProduct(Guid productId)
        {
            ProductId = productId;
        }

        public Guid ProductId { get; }
    }
}