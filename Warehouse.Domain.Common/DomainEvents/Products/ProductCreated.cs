﻿using DddInfrastructure;

namespace Warehouse.Domain.Common.DomainEvents.Products
{
    public class ProductCreated : AggregateRootCreatedDomainEvent
    {
        public string Name { get; }

        public ProductCreated(string name)
        {
            Name = name;
        }
    }
}