﻿using DddInfrastructure;

namespace Warehouse.Domain.Common.DomainEvents.Products
{
    public class NewPromotionActivated : DomainEvent
    {
        public NewPromotionActivated(int percentDiscount, string promoCode)
        {
            PercentDiscount = percentDiscount;
            PromoCode = promoCode;
        }

        public int PercentDiscount { get; }
        public string PromoCode { get; }
    }
}