﻿using System;

namespace DddInfrastructure
{
    public abstract class DomainEntity : IIdentifiable
    {
        public Guid Id { get; set; }
    }
}
