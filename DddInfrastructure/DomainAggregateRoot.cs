﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace DddInfrastructure
{
    public abstract class DomainAggregateRoot : DomainEntity
    {
        protected DomainAggregateRoot(IAggregateRootCreatedDomainEvent aggregateRootCreatedDomainEvent)
        {
            Emit(aggregateRootCreatedDomainEvent);
        }

        readonly List<IDomainEvent> _pendingEvents = new List<IDomainEvent>();

        protected void Emit(IDomainEvent domainEvent)
        {
            _pendingEvents.Add(domainEvent);
        }

        public IReadOnlyList<IDomainEvent> PendingEvents => _pendingEvents.ToImmutableList();
    }
}