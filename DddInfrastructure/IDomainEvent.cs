﻿using System;

namespace DddInfrastructure
{
    public interface IDomainEvent
    {
        Guid Id { get; }
        DateTimeOffset EmittedTime { get; }
    }
}