﻿using System;

namespace DddInfrastructure
{
    public abstract class DomainEvent : IDomainEvent
    {
        public Guid Id { get; } = Guid.NewGuid();
        public DateTimeOffset EmittedTime { get; } = DateTimeOffset.Now;
    }
}