﻿using System.Threading.Tasks;

namespace DddInfrastructure
{
    public interface IDomainEventBus
    {
        Task PublishAsync(IDomainEvent domainEvent);
    }
}