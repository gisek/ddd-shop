﻿using System.Collections.Generic;
using System.Linq;

namespace DddInfrastructure
{
    // source: https://github.com/vkhorikov/CSharpFunctionalExtensions
    public abstract class DomainValueObject
    {
        protected abstract IEnumerable<object> GetEqualityComponents();

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            return GetEqualityComponents().SequenceEqual(((DomainValueObject)obj).GetEqualityComponents());
        }

        public override int GetHashCode()
        {
            return GetEqualityComponents().Aggregate(1, (current, obj) => current * 23 + (obj != null ? obj.GetHashCode() : 0));
        }

        public static bool operator ==(DomainValueObject a, DomainValueObject b)
        {
            if ((object)a == null && (object)b == null)
                return true;
            if ((object)a == null || (object)b == null)
                return false;
            return a.Equals(b);
        }

        public static bool operator !=(DomainValueObject a, DomainValueObject b)
        {
            return !(a == b);
        }
    }

}
