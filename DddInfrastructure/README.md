﻿# About

This is a set of tools needed for Domain Driven Design implementation
It's agnostic against:
* database
* ORM
* message broker
* web framework