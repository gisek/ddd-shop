﻿using System.Threading.Tasks;

namespace DddInfrastructure
{
    public interface ICommandBus
    {
        Task Send(ICommand command);
    }
}