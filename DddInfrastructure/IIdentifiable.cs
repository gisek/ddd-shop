﻿using System;

namespace DddInfrastructure
{
    public interface IIdentifiable
    {
        Guid Id { get; set; }
    }
}