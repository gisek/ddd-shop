﻿using System.Threading.Tasks;

namespace DddInfrastructure
{
    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        Task Handle(TCommand command);
    }
}